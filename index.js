// all methods are chainable

class Product {
	constructor(name,price){

		this.name = name;
		// this.price = price;
		if(typeof price !== 'number'){
			this.price = undefined
		} else {
			this.price = price
		}
		this.isActive = true;

	}

	// Archive - from true to false
	archive(newStatus){
		if(newStatus === false){
			this.isActive = newStatus;
		} else {
			console.log("only accepts 'false' boolean")
		}
		return this
	}

	updatePrice(newPrice){
		if(typeof newPrice === 'number'){
			this.price = newPrice;
		} else {
			console.log("Please enter a numerical value")
		}
		return this
	}
}

let prodA = new Product("soap",95);
let prodB = new Product("shampoo",250);
console.log(prodA);
console.log(prodB);


class Cart {
	constructor(){

		this.content = [];
		this.totalAmount = 0;
	}

	addToCart(product,quantity){
		let item = {
			"product": product,
			"quantity": quantity
		}
		this.content.push(item);
		return this
	}
	showCartContents(){
		console.log(this.content);
		return this
	}
	clearCartContents(){
		let newArr = []
		this.content = newArr;
		this.totalAmount = 0;
		return this
	}

// iterates over every product in the cart, multiplying product price with their respective quantities. sums up all results and sets value as totalAmount
	computeTotal(){
		let itemPrice = 0
		this.content.forEach(item =>{
			// console.log(item.product.price);
			// console.log(item.quantity);
			itemPrice += item.product.price * item.quantity;
		});

		this.totalAmount = itemPrice
		return this
	}
}

let cart1 = new Cart();
cart1.addToCart(prodA,2);
cart1.addToCart(prodB,1);


class Customer {

	constructor(email){
		this.email = email;
		this.cart = cart1;
		this.orders = [];
	}

	checkout(){
		if(this.cart.content.length !== 0){
			let orderDetails = {
				"products": this.cart.content,
				"totalAmount": this.cart.totalAmount
				}
			this.orders.push(orderDetails);
		} else {
			console.log("Your cart is empty");
		}
		return this
	}
}

let john = new Customer("john@gmail.com");